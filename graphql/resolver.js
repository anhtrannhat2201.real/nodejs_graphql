const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();
const graphqlResolver = {
  getUser: async () => {
    try {
      let data = await prisma.user.findMany();
      return data;
    } catch (error) {
      return error;
    }
  },
  getFood: async () => {
    try {
      let dataFood = await prisma.food.findMany({
        include: { food_type: true },
      });
      return dataFood;
    } catch (error) {
      return error;
    }
  },
  // nhận tham số id
  createUser: async (agru) => {
    let { full_name, email, pass_word } = agru;
    await prisma.user.create({ data: { full_name, email, pass_word } });
    return { full_name, email, pass_word };
  },
};
module.exports = graphqlResolver;
