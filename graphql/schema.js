const { buildSchema } = require("graphql");
const graphqlShema = buildSchema(`
    type user{
        user_id:ID
        full_name:String!
        email:String!
        pass_word:String!
    }
    type food {
        food_id:ID       
        food_name:String!  
        image:String!  
        price:Int!
        desc:String! 
        type_id:Int!
        food_type:food_type
      }
      type food_type {
        type_id:ID
        type_name:String!
      }

    type RootQuery{
        getUser:[user],
        getFood:[food]
    }
    type RootMutation{
        createUser(user_id:ID,full_name:String,email:String,pass_word:String):String,
        
    }
    schema{
        query:RootQuery
        mutation:RootMutation
    }
`);
module.exports = graphqlShema;
