const express = require("express");
const app = express();
app.use(express.json());
app.use(express.static("."));
app.listen(8080);
const { graphqlHTTP } = require("express-graphql");
const graphqlShema = require("./graphql/schema");
const graphqlResolver = require("./graphql/resolver");
app.use(
  "/grap",
  graphqlHTTP({
    schema: graphqlShema, // nơi nhận vào các đối tượng được quản lý bên thư mục graphql>file schema.js
    rootValue: graphqlResolver, //nơi xử lý các login, chức năng,... được quản lý bên thư mục graphql>file resolver.js
    graphiql: true,
  })
);
